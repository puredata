This is DesireData 0.39.A.pre3 (2006.11.27)

See INSTALL.txt for installation instructions.

This is the README file for DesireData, a free (libre) real-time computer
programming language interpreter focused on realtime audio and video.
You can get DesireData from http://desiredata.goto10.org/

If you have qustions about Pd, or if you wish to be notified of releases,
check the Pd mailing list: http://iem.mhsg.ac.at/mailinglists/pd-list/

All files in the DesireData distribution are supposed to be labeled with
copyright/license notices. There is no copyright nor license that you may 
assume for files for which you do not know who is the author. If you find
 such files, please contact matju and/or chun. (This is different than 
Miller's distribution, which supposes a general copyright on all files it
contains.)

For information on usage and redistribution, and for a DISCLAIMER OF ALL
WARRANTIES, see the files "COPYING" and "COPYING.desire-client.txt", which 
cover licensing of the two parts of DesireData. (Note that tcl/tk, expr,
and some other files are copyrighted separately).

Mathieu and Chun thank: Thomas Grill, Tim Blechmann, all translators, ...

Miller thanked: Harry Castle, Krzysztof Czaja, Mark Danks,
Christian Feldbauer, Guenter Geiger, Kerry Hagan, Trevor Johnson, Fernando
Lopez-Lezcano, Adam Lindsay, Karl MacMillan, Thomas Musil, Toshinori Ohkouchi,
Winfried Ritsch, Vibeke Sorensen, Rand Steiger, Hans-Christoph Steiner,
Shahrokh Yadegari, David Zicarelli, Iohannes Zmoelnig, and probably many others
for contributions of code, documentation, ideas, and expertise.  This work has
received generous support from the Intel Research Council.

Further Acknowledgements:
 - devel_0_39 lockfree fifos adapted from midishare: Copyright � Grame 1999
   Grame Research Laboratory, 9, rue du Garet 69001 Lyon - France
   grame@rd.grame.fr

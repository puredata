#!/usr/bin/env tclsh
# Catalan (catal�) translations for PureData
# $Id: catala.tcl,v 1.1.2.5 2006-10-13 16:00:56 matju Exp $
# by N�ria Verges

say file "Fitxer"
 say new_file "Nou Fitxer"
 say open_file "Obrir Fitxer..."
 say pdrc_editor "Editor de .pdrc"
 say send_message "Enviar Missatge..."
 say paths "Camins..."
 say close "Tancar"
 say save "Desar"
 say save_as "Guardar com a..."
 say print "Imprimir..."
 say quit "Sortir"

say edit "Editar"
 say undo "Desfer"
 say redo "Refer"
 say cut "Tallar"
 say copy "Copiar"
 say paste "Enganxar"
 say duplicate "Duplicar"
 say select_all "Seleccionar-ho tot"
 say text_editor "Editor de text..."
 say tidy_up "Netejar"
 say edit_mode "Mode d'edici�"

say view "Veure"
 say reload "Recarregar"
 say redraw "Redissenyar"

say find "Trobar"
 say find_again "Trobar novament"
 say find_last_error "Trobar l'�ltima errada"

say put "Posar"

say media "Media"
 say audio_on "Audio ON"
 say audio_off "Audio OFF"

say window "Finestra"

say help "Ajuda"
 say about "Sobre..."
 say pure_documentation "Pure Documentaci�..."
 say class_browser "Cercador de Classes..."

### Main Window

say in "in"
say out "out"
say audio "Audio"
say meters "Meters"
say io_errors "Errades d'E/S"


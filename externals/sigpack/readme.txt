sIgpAck version 0.03

signal object library for pure-data
win-32 only

objects included
sp.chop~ sp.decimate~ sp.diode~ sp.foldback~ sp.foldover~ sp.freqdiv~
sp.freqshift~ sp.harmgen~ sp.rectify~ sp.round~ sp.saturate~ sp.sieve~
sp.split~ sp.ustep~ sp.vowel~

this is software with absolutely no warranty
use it at your own risk

contact
<weiss@weiss-archiv.de>
<http://www.weiss-archiv.de>
This is the readme for "gphoto" a libgphoto2 interface external for PD. 

popup is Copyright Ben Bogart 2009

If you have any questions/comments you can reach the author at ben@ekran.org.

This program is distributed under the terms of the GNU General Public 
License 

popup is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or 
(at your option) any later version.

popup is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License
along with popup; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

USAGE:

Put the binary in your extra folder.
Put the helpfile in your 5.reference folder.

TODO:
* Add a function to wrap gp_error results, and set busy to 0.
* Add getFile and getFiles functions for PTP file transfer functionality.

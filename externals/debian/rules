#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This is the debhelper compatibility version to use.
export DH_COMPAT=3



ifneq (,$(findstring debug,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -g
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

orig: clean
	(cd ..;tar czvf pd-externals_00.`date +%Y%m%d`.orig.tar.gz pd-externals)


configure: configure-stamp
configure-stamp:
	dh_testdir
	# Add here commands to configure the package.

	touch configure-stamp


build: build-stamp

FOOBUILD = PD_DIR=../../build/include \
                          FLEXTPATH=../../grill/flext/pd-linux \
	                  FLEXTINC=../../grill/flext/source \
                          FLUIDLIB=" -lfluidsynth"

build-stamp: configure-stamp 
	dh_testdir

	# Add here commands to compile the package.
	# Build the common externals

	(cd build/linux; $(MAKE))

        # Build zexy

	(cd zexy/src; $(MAKE) -f makefile.linux LINCLUDE="-I ../../build/include")

	# Build flext

	(cd grill/flext; $(MAKE) -f makefile.pd-linux PDPATH=../../build/include STK="" )

	# Flext externals
	#

	(make -C footils/fluid -k  $(FOOBUILD))
	(make -C footils/shabby -k  $(FOOBUILD))
	(make -C footils/syncgrain -k  $(FOOBUILD))
 
	# OSC

	(cd OSCx/libOSC;$(MAKE))
	(cd OSCx/OSC; $(MAKE))

	# build cyclone
	-mkdir miXed/bin
	(cd miXed/cyclone; make PD_DIR='../../build/include')


	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp configure-stamp

	# Remove the garbage that ppl put in CVS
	-rm `find . -name "*.pd_linux"`
	-rm `find . -name "*.lib"`
	-rm `find . -name "*.o"`
	-rm `find . -name "*.dll"`
	-rm `find . -name "*.obj"` 

	# fix up wrong permissions

	chmod 644 `find . -name "*.h"`
	chmod 644 `find . -name "*.c"`
	chmod 644 `find . -name "*.pd"`

	# Add here commands to clean up after the build process.
	-(cd build/linux;$(MAKE) clean)

	(cd zexy/src; $(MAKE) -f makefile.linux  clean)

	(cd grill/flext; $(MAKE) -f makefile.pd-linux clean)

	-(cd footils; make clean)

	-(cd OSCx/libOSC;$(MAKE) clean)
	-(cd OSCx/OSC; $(MAKE) pd_linux_clean)
#
#  Clean cyclone
#
	(cd miXed/cyclone; make clean)
	-rm miXed/bin/cyclist

	dh_clean

FOOINSTALL = FLEXTPATH=../../grill/flext/pd-linux \
	        FLEXTINC=../../grill/flext/source \
		DESTDIR=$(CURDIR)/debian/tmp INSTPATH=$(CURDIR)/debian/tmp/usr/lib/pd/extra

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	# Add here commands to install the package into debian/pd-externals.

	(cd build/linux; $(MAKE) install DESTDIR=$(CURDIR)/debian/tmp)
	(rm -r `find $(CURDIR)/debian/tmp/ -name CVS`)

# Install zexy

	(cd zexy/src; $(MAKE) -f makefile.linux install DESTDIR=$(CURDIR)/debian/tmp \
	                              INSTALL_PREFIX=/usr/ \
				      PDLIBDIR=lib/pd )
#	ln -s zexy $(CURDIR)/debian/tmp/usr/lib/pd/doc/5.reference/help-zexy

# Install flext

	(cd grill/flext; $(MAKE) -f makefile.pd-linux install INSTDIR=$(CURDIR)/debian/tmp/usr/lib/pd/flext)

# Install footils ... this is quite messy still, have to find a better solution

	(make -C footils/fluid -k  $(FOOINSTALL) install)
	(make -C footils/shabby -k  $(FOOINSTALL) install)
	(make -C footils/syncgrain -k  $(FOOINSTALL) install)
	install -m 644 footils/fluid/pd/fluid.pd $(CURDIR)/debian/tmp/usr/lib/pd/doc/5.reference/fluid-help.pd
	install -m 644 footils/shabby/shaffy~-help.pd $(CURDIR)/debian/tmp/usr/lib/pd/doc/5.reference
	install -m 644 footils/shabby/shabby~.pd_linux $(CURDIR)/debian/tmp/usr/lib/pd/doc/5.reference
	-rm $(CURDIR)/debian/tmp/usr/lib/pd/extra/fiiwu*

# Install OSC

	install OSCx/OSC/OSC.pd_linux $(CURDIR)/debian/tmp/usr/lib/pd/extra
	install -m 644 OSCx/doc/*help.pd $(CURDIR)/debian/tmp/usr/lib/pd/doc/5.reference

# Install cyclone

	mkdirhier $(CURDIR)/debian/tmp/usr/lib/pd/externs/cyclone
	install $(CURDIR)/miXed/bin/*.pd_linux $(CURDIR)/debian/tmp/usr/lib/pd/externs/cyclone/
	install -m 644  $(CURDIR)/miXed/test/cyclone/*.pd  $(CURDIR)/miXed/doc/*/cyclone/*.* \
				$(CURDIR)/debian/tmp/usr/lib/pd/doc/5.reference


# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_movefiles
#	dh_installdebconf	
	dh_installdocs
	dh_installexamples
	dh_installmenu
#	dh_installlogrotate
#	dh_installemacsen
#	dh_installpam
#	dh_installmime
#	dh_installinit
	dh_installcron
	dh_installman
	dh_installinfo
#	dh_undocumented
	dh_installchangelogs 
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
#	dh_makeshlibs
	dh_installdeb
#	dh_perl
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
